from __future__ import absolute_import, unicode_literals
from celery import shared_task
from core.models import FarmersCallingTime, FarmersCallLog, Farmers
from datetime import datetime, timedelta
from core.utils import trigger_call
from structlog import get_logger
from django.conf import settings


@shared_task(name="scheduling_calls")
def scheduling_calls():
    now = datetime.now()
    day = now.today().weekday()
    five_min_ago = now - timedelta(minutes=10)
    logger = get_logger(__name__).bind(
        action="scheduling_calls",
        now=now,
        day=day,
        five_min_ago=five_min_ago
    )
    logger.info("start")
    farmers = FarmersCallingTime.objects.filter(
        time__gte=five_min_ago, time__lte=now.time(), day=day)

    logger.info("farmers_to_call_count", number=farmers.count())

    for farmer in farmers:
        # check if we had intiated the call before in the last 10min
        hour_ago = now - timedelta(hours=1)
        if not FarmersCallLog.objects.filter(
                farmer=farmer.farmer,
                createdAt__gte=hour_ago).exists():

            # check that we have not exceeded the number of
            # times we need to call in a day
            if FarmersCallLog.objects.filter(
                    farmer=farmer.farmer, createdAt__day=day).count() \
                    < settings.CALLING_TIMES_IN_DAY:
                # initiate a call
                logger.info("schedule", farmer=farmer.farmer.identity)
                call_farmer.delay(farmer.farmer.identity)
            else:
                logger.info("farmer_already_called")
    logger.info("end")
    return


@shared_task(name="calling_farmers")
def call_farmer(farmer_identity):
    logger = get_logger(__name__).bind(
        action="call_farmer_task",
        farmer=farmer_identity
    )
    logger.info('start')
    result = trigger_call(farmer_identity)
    logger.info('end', result=result)

    if result:
        farmer = Farmers.objects.get(identity=farmer_identity)
        FarmersCallLog.objects.create(farmer=farmer)
