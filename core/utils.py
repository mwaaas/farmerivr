from core.models import Farmers, FarmersCallingTime, \
    FarmerSupport
from annoying.functions import get_object_or_None
from datetime import time
from django.conf import settings
from datetime import datetime
from structlog import get_logger
import requests
import json


def get_user(ussd_req):
    user_status = dict(
        registered=False,
        current_module='0'
    )
    farmer = get_object_or_None(
        Farmers,
        identity=ussd_req.phone_number)
    if farmer:
        user_status['registered'] = True
        user_status['current_module'] = farmer.current_module
    return user_status


def _create_farmer_call_time(phone_number, hour_to_call, initiated_by,
                             days=settings.DEFAULT_CALLING_DAYS):
    farmer = Farmers.objects.get(
        identity=phone_number
    )
    for day in days:
        FarmersCallingTime.objects.create(
            farmer=farmer,
            time=time(hour=int(hour_to_call)),
            day=day
        )

        # this should only be used for testing purpose
        # calling the same day multiple times
        FarmersCallingTime.objects.create(
            farmer=farmer,
            time=time(hour=int(hour_to_call + 2)),
            day=day
        )

        FarmersCallingTime.objects.create(
            farmer=farmer,
            time=time(hour=int(hour_to_call + 4)),
            day=day
        )


def reg_user(ussd_req):
    Farmers.objects.create(
        identity=ussd_req.phone_number
    )

    # In case the farmer drops call before he or she selects
    # the preferred time to call. Use the current
    # hour he called as the preferred time
    current_hour = datetime.now().hour
    _create_farmer_call_time(ussd_req.phone_number, current_hour, 'default')


def reg_time_to_call(ussd_req):
    farmer = Farmers.objects.get(
        identity=ussd_req.phone_number
    )

    # delete all the time for calling that are initialized by default
    FarmersCallingTime.objects.filter(farmer=farmer,
                                      initiated_by='default').delete()

    hour_to_call = ussd_req.session['hour_to_call']
    _create_farmer_call_time(ussd_req.phone_number,
                             hour_to_call, 'farmer')


def upgrade_lesson(farmer_identify, lesson_number):
    farmer = Farmers.objects.get(identity=farmer_identify)
    farmer.current_module = lesson_number
    farmer.save()


def flag_user(ussd_request, issue_type):
    farmer = Farmers.objects.get(
        identity=ussd_request.phone_number)
    FarmerSupport.objects.create(farmer=farmer,
                                 issue_type=issue_type)


def resolve_issue(ussd_request, issue_type):
    farmer = Farmers.objects.get(
        identity=ussd_request.phone_number)
    farmer_support = get_object_or_None(
        FarmerSupport,
        farmer=farmer,
        issue_type=issue_type,
        issue_status=True
    )

    if farmer_support:
        farmer_support.issue_status = False
        farmer_support.save()


def lesson_0_a_flag_user(ussd_request):
    flag_user(ussd_request, FarmerSupport.lesson_0_a)


def lesson_0_a_resolve(ussd_request):
    resolve_issue(ussd_request, FarmerSupport.lesson_0_a)


def upgrade_lesson_1(ussd_req):
    upgrade_lesson(ussd_req.phone_number, 1)
    lesson_0_a_resolve(ussd_req)


def upgrade_lesson_2(ussd_req):
    upgrade_lesson(ussd_req.phone_number, 2)


def upgrade_lesson_3a(ussd_req):
    upgrade_lesson(ussd_req.phone_number, '3a')


def upgrade_lesson_3b(ussd_req):
    upgrade_lesson(ussd_req.phone_number, '3b')


def upgrade_lesson_4(ussd_req):
    upgrade_lesson(ussd_req.phone_number, '4')


def upgrade_lesson_5(ussd_req):
    upgrade_lesson(ussd_req.phone_number, '5')


def upgrade_lesson_6(ussd_req):
    upgrade_lesson(ussd_req.phone_number, '6')


def upgrade_lesson_7(ussd_req):
    upgrade_lesson(ussd_req.phone_number, '7')


def trigger_call(phone_number):
    logger = get_logger(__name__).bind(
        action="trigger_call"
    )
    url = "https://voice.africastalking.com/call"

    payload = {"username": settings.AFRICASTALKING_USERNAME,
               "from": '+254711082119',
               "to": phone_number
               }
    headers = {
        'Accept': "application/json",
        'apikey': settings.AFRICASTALKING_API_KEY
    }

    logger.info("cal_request", url=url, data=payload, headers=headers)
    response = requests.post(url, data=payload, headers=headers)
    logger.info("call_response", status_code=response.status_code,
                content=response.content)

    if response.status_code == 200:
        data = json.loads(response.content.decode())
        logger.info("call_response_json", status_code=response.status_code,
                    content=data)
        if data['errorMessage'] == 'None':
            return True
    return False
