from django.contrib import admin
from core.models import Farmers, FarmersCallingTime, FarmersCallLog
from django.contrib.admin import ModelAdmin, StackedInline, TabularInline
from django.contrib.admin.options import InlineModelAdmin


class FarmersCallingTimeAdmin(TabularInline):
    model = FarmersCallingTime
    extra = 0
    can_delete = False


class FarmerAdmin(ModelAdmin):
    list_display = ('identity', 'current_module')
    list_filter = ('identity', 'current_module',
                   'created_at', 'updated_at')
    inlines = [FarmersCallingTimeAdmin]


class FarmerCallingTimeAdmin(ModelAdmin):
    list_display = ('farmer', 'time', 'day', 'initiated_by')


class FarmersCallLogAdmin(ModelAdmin):
    list_display = ('farmer', 'createdAt', 'updatedAt')


# Register your models here.
admin.site.register(Farmers, FarmerAdmin)
admin.site.register(FarmersCallingTime, FarmerCallingTimeAdmin)
admin.site.register(FarmersCallLog, FarmersCallLogAdmin)
