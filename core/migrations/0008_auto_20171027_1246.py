# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-27 12:46
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20171020_0752'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='farmers',
            options={'verbose_name_plural': 'Farmers'},
        ),
    ]
