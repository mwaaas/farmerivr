# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-19 09:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20171016_0336'),
    ]

    operations = [
        migrations.CreateModel(
            name='FarmersCallLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('createdAt', models.DateTimeField(auto_created=True)),
                ('updatedAt', models.DateTimeField(auto_now_add=True)),
                ('farmer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Farmers')),
            ],
        ),
    ]
