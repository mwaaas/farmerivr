# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-27 13:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20171027_1246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='farmers',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='farmers',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
