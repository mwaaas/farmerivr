# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-02 12:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20171002_1101'),
    ]

    operations = [
        migrations.AddField(
            model_name='farmersupport',
            name='issue_type',
            field=models.CharField(choices=[('lesson_0_a', 'lesson_0_a')], default='mwaside', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='farmersupport',
            unique_together=set([('issue_type', 'farmer')]),
        ),
    ]
