from django.db import models
from django.core.exceptions import ValidationError


class Farmers(models.Model):
    identity = models.CharField(max_length=255,
                                unique=True, db_index=True)
    current_module = models.CharField(max_length=255, default='0')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Farmers"


def validate_day(value):
    if value < 0 or value > 6:
        raise ValidationError(
            '%(value)s is not a valid day',
            params={'value': value},
        )


class FarmersCallingTime(models.Model):
    time = models.TimeField()
    farmer = models.ForeignKey(Farmers)
    day = models.IntegerField(validators=[validate_day])
    initiated_by = models.CharField(max_length=255)

    class Meta:
        unique_together = ('time', 'farmer', 'day')


class FarmersCallLog(models.Model):
    farmer = models.ForeignKey(Farmers)
    createdAt = models.DateTimeField(auto_created=True, auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now_add=True)


class FarmerSupport(models.Model):
    lesson_0_a = 'lesson_0_a'
    ISSUE_TYPES = (
        ('lesson_0_a', 'lesson_0_a'),
    )
    issue_status = models.BooleanField(default=True)
    farmer = models.ForeignKey(Farmers)
    issue_type = models.CharField(
        max_length=255,
        choices=ISSUE_TYPES
    )

