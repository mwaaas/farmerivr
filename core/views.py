"""View for the farmer IVR."""
import os
from django.http import HttpResponse
from ussd.core import UssdView, UssdRequest
from django.conf import settings
from core.tasks import call_farmer
from structlog import get_logger


BASE_FORMAT = '<?xml version="1.0" encoding="UTF-8"?>' \
              '<Response>{content}</Response>'

GET_DIGITS_FORMAT = BASE_FORMAT.format(
    content=(
        '<GetDigits timeout="5">{content}</GetDigits>'
        '<GetDigits timeout="5">{content}</GetDigits>'
        '<GetDigits timeout="5">{content}</GetDigits>'
    )
)

SAY_CONTENT = '<Say>{text}</Say>'
SAY_FORMAT = GET_DIGITS_FORMAT.format(content=SAY_CONTENT)
SAY_END_FORMAT = BASE_FORMAT.format(content=SAY_CONTENT)

PLAY_CONTENT = '<Play url="' + \
               settings.USSD_CLIPS_ENDPOINT + \
               '{text}.wav"/>'
PLAY_FORMAT = GET_DIGITS_FORMAT.format(content=PLAY_CONTENT)
PLAY_END_FORMAT = BASE_FORMAT.format(content=PLAY_CONTENT)


class AfricasTalkingIvrGateway(UssdView):
    """Custom IVR gateway."""

    customer_journey_conf = os.path.join(
        settings.BASE_DIR, 'core/customer-journey.yml')
    customer_journey_namespace = 'ivr-customer-journey'

    @staticmethod
    def post(req):
        logger = get_logger(__name__)
        logger.info("raw_ussd_request", **req.data)
        """Handle POST to the view."""
        # if its client is calling terminate and call back
        if req.data['direction'] == 'Inbound':
            logger.info("terminate_can_callback")
            call_farmer.delay(req.data['callerNumber'])
            return HttpResponse()
        return UssdRequest(
            phone_number=req.data['callerNumber'].strip('+'),
            session_id=req.data['sessionId'],
            ussd_input=req.data.get('dtmfDigits', ''),
            language=req.data.get('language', 'en'),
            **req.data
        )

    def ussd_response_handler(self, ussd_response):
        """Handle USSD response in the view."""
        uss_text = str(ussd_response).replace('\n', '')
        if ussd_response.status:
            content = SAY_FORMAT
            if 'play:' in uss_text:
                content = PLAY_FORMAT
                uss_text = uss_text.split(':')[-1]
        else:
            content = SAY_END_FORMAT
            if 'play:' in uss_text:
                content = PLAY_END_FORMAT
                uss_text = uss_text.split(':')[-1]

        content = content.format(text=uss_text)
        self.logger.info("ivr_response", content=content)
        return HttpResponse(content)
