from django.test import LiveServerTestCase
import uuid
from .util import IvrTestClient, CONVERSATION_FORMAT, \
    END_CONVERSATION_FORMAT
from core.models import Farmers, FarmersCallingTime
from django.conf import settings
from datetime import datetime


class FirstTimeCall(LiveServerTestCase):

    first_conversation = "Press one " \
                         "if you would like to be called " \
                         "by this number or two by a different number"

    requesting_hour_to_call = "Enter the hour you would " \
                              "like the system to call you in 24 hour system"

    ending_conversation = "Good bye"

    def test_calling_for_the_first_time_using_your_phone(self):
        phone_number = '200'
        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        ivr_resp = client.send('')

        self.assertEqual(CONVERSATION_FORMAT.format(text=self.first_conversation),
                         ivr_resp.content.decode()
                         )

        # select one
        ivr_resp = client.send('1')

        # check the number has been saved in the system.
        self.assertTrue(
            Farmers.objects.filter(identity=phone_number).exists()
        )

        farmer = Farmers.objects.get(identity=phone_number)

        # check we are requesting for hour to call
        self.assertEqual(CONVERSATION_FORMAT.format(
            text=self.requesting_hour_to_call),
            ivr_resp.content.decode())

        # select to be called at 9
        ivr_resp = client.send('9')

        # check we have saved the day to be called.
        calling_days = settings.DEFAULT_CALLING_DAYS
        for day in calling_days:
            self.assertTrue(
                FarmersCallingTime.objects.filter(farmer=farmer, day=day, time__hour=9).exists()
            )

        # ending conversation
        self.assertEqual(
            END_CONVERSATION_FORMAT.format(
                text=self.ending_conversation
            ),
            ivr_resp.content.decode()
        )

    def test_farmer_dropping_call_before_selecting_hour(self):
        phone_number = '202'

        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)

        # dial in
        client.send('')
        client.send('1')

        # check the number has been saved in the system.
        self.assertTrue(
            Farmers.objects.filter(identity=phone_number).exists()
        )

        farmer = Farmers.objects.get(identity=phone_number)
        current_hour = datetime.now().hour
        # check we have saved the day to be called.
        calling_days = settings.DEFAULT_CALLING_DAYS
        for day in calling_days:
            self.assertTrue(
                FarmersCallingTime.objects.filter(
                    farmer=farmer, day=day, time__hour=current_hour).exists()
            )


    # todo: write test to check for validation
    def testing_validation(self):
        pass
