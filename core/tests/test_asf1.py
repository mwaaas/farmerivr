from django.test import LiveServerTestCase
import uuid
from .util import IvrTestClient, \
    CONVERSATION_FORMAT, END_CONVERSATION_FORMAT, \
    PLAY_CONVERSATION_FORMAT
from core.models import Farmers
from django.conf import settings


class TestingASF1(LiveServerTestCase):
    maxDiff = None

    first_conversation = settings.USSD_CLIPS_ENDPOINT + "asf1.wav"

    incorrect_conversation = "No, sometimes some pigs die " \
                             "without showing symptoms and pigs " \
                             "that recover will carry the virus " \
                             "without showing signs"

    correct_conversation = "Yes, that is right"

    def setUp(self):
        # register all numbers we are going to use
        for i in (200, 202, 400):
            Farmers.objects.create(
                identity=str(i),
                current_module=1
            )

    def test_correct_answer(self):
        phone_number = '200'

        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        ivr_resp = client.send('')

        self.assertEqual(
            PLAY_CONVERSATION_FORMAT.format(text=self.first_conversation),
            ivr_resp.content.decode()
        )

        # enter two which is the correct answer
        ivr_resp = client.send('2')

        self.assertEqual(
            END_CONVERSATION_FORMAT.format(text=self.correct_conversation),
            ivr_resp.content.decode()
        )

        # ensure farmer has been moved to the next module
        farmer = Farmers.objects.get(identity=phone_number)
        self.assertEqual('2', farmer.current_module)

    def test_incorrect_answer(self):
        phone_number = '202'

        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        ivr_resp = client.send('')

        # enter one which is the incorrect answer
        ivr_resp = client.send('1')

        self.assertEqual(
            END_CONVERSATION_FORMAT.format(text=self.incorrect_conversation),
            ivr_resp.content.decode()
        )

