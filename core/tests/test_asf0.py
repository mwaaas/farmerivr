from django.test import LiveServerTestCase
import uuid
from .util import IvrTestClient, \
    CONVERSATION_FORMAT, END_CONVERSATION_FORMAT
from core.models import Farmers, FarmerSupport


class TestingLearnerTraining(LiveServerTestCase):
    def setUp(self):

        # Register all the numbers we are going to use here
        for i in (200, 202, 400):
            Farmers.objects.create(identity=str(i))

    first_conversation = "Thank you for registering, Welcome to this " \
                         "learning calls on African swine fever, " \
                         "during the lesson we will ask you to answer " \
                         "some simple but important questions, " \
                         "to answer the questions you will press the " \
                         "correct number on your phone. Let's try, " \
                         "if you understand these instructions on your" \
                         " phone press the number one now."

    correct_conversation = 'Now you can provide answers you can answer this simple question. ' \
                           'These learning calls are about ' \
                           'African swine fever. If you think the ' \
                           'answer is yes press one, ' \
                           'if you think no then press 2. Choose now.'

    correction_conversation = "When you are asked a question " \
                              "you can respond by pressing the numbers " \
                              "on your phone in the same way you dial " \
                              "a phone number. Let's try, can you press " \
                              "on your phone the number one now"

    last_conversation = 'Good, now you can get the most of these ' \
                        'learning calls. ' \
                        'Thank you again for registering and ' \
                        'call back soon.'

    error_conversation = 'Sorry, we cannot assist at the ' \
                         'moment please try again at another time.'

    def test_happy_case(self):
        phone_number = '200'
        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        ivr_resp = client.send('')

        self.assertEqual(
            CONVERSATION_FORMAT.format(text=self.first_conversation),
            ivr_resp.content.decode())

        # enter the correct answer which is one
        ivr_resp = client.send('1')

        self.assertEqual(
            CONVERSATION_FORMAT.format(text=self.correct_conversation),
            ivr_resp.content.decode()
        )

        # enter one for answer yes
        ivr_resp = client.send('1')

        self.assertEqual(
            END_CONVERSATION_FORMAT.format(text=self.last_conversation),
            ivr_resp.content.decode()
        )

        # ensure the farmer has been moved to module two
        self.assertEqual('1',
                         Farmers.objects.get(
                             identity=phone_number).current_module)

    def test_flagged_user(self):
        phone_number = '400'
        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        ivr_resp = client.send('')

        self.assertEqual(
            CONVERSATION_FORMAT.format(text=self.first_conversation),
            ivr_resp.content.decode())

        # enter the incorrect answer 3 times
        # which is anything else apart from one

        for i in range(3):
            ivr_resp = client.send('5')

            self.assertEqual(
                CONVERSATION_FORMAT.format(text=self.correction_conversation),
                ivr_resp.content.decode()
            )

        # entering incorrect answer for the 4 time
        ivr_resp = client.send('5')
        self.assertEqual(
            END_CONVERSATION_FORMAT.format(text=self.error_conversation),
            ivr_resp.content.decode()
        )

        # ensure user is flagged.
        self.assertTrue(
            FarmerSupport.objects.filter(
                farmer=Farmers.objects.get(identity=phone_number),
                issue_status=True,
                issue_type=FarmerSupport.lesson_0_a
            ).exists()
        )

        # check user is unflagged when he calls again
        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        client.send('')
        client.send('1')
        client.send('1')

        # ensure user is un flagged.
        self.assertFalse(
            FarmerSupport.objects.filter(
                farmer=Farmers.objects.get(identity=phone_number),
                issue_status=True,
                issue_type=FarmerSupport.lesson_0_a
            ).exists()
        )

    def test_selecting_wrong_series(self):
        client = IvrTestClient('202', str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        client.send('')
        client.send('1')
        response = client.send('2')

        self.assertEqual(
            END_CONVERSATION_FORMAT.format(
                text='Sorry, this series of learning calls are '
                     'about African swine fever.'
            ),
            response.content.decode()
        )
