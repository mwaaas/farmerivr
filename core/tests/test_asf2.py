from django.test import LiveServerTestCase
import uuid
from .util import IvrTestClient, \
    END_CONVERSATION_FORMAT, PLAY_CONVERSATION_FORMAT, CONVERSATION_FORMAT
from core.models import Farmers
from django.conf import settings


class TestingASF2(LiveServerTestCase):
    maxDiff = None

    first_conversation = settings.USSD_CLIPS_ENDPOINT + "asf2.wav"

    correct_second_conversation = "Yes, an increased appetite is not a sign, " \
                                  "it is the opposite where the animals have " \
                                  "a lack of appetite If I see one of my pigs with " \
                                  "reddish skin and blood in the diarrhoea I should " \
                                  "report it to the District Veterinary Officer of area vet. " \
                                  "Press one for Yes or two for No."

    def setUp(self):
        # register all numbers we are going to use
        for i in (200, 201, 202, 203, 204, 205, 206, 207):
            Farmers.objects.create(
                identity=str(i),
                current_module=2
            )

    def test_correct_answer(self):
        phone_number = '200'

        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        ivr_resp = client.send('')

        self.assertEqual(
            PLAY_CONVERSATION_FORMAT.format(text=self.first_conversation),
            ivr_resp.content.decode()
        )

        # enter the correct answer which is four
        ivr_resp = client.send('4')

        self.assertEqual(
            CONVERSATION_FORMAT.format(text=self.correct_second_conversation),
            ivr_resp.content.decode()
        )

        # enter the correct answer which is one
        ivr_resp = client.send('1')

        expected_text = 'Yes, reporting to the District veterinary ' \
                        'Office will help stop the spread of African swine fever'

        self.assertEqual(
            END_CONVERSATION_FORMAT.format(text=expected_text),
            ivr_resp.content.decode()
        )

        # ensure farmer has been moved to the next module
        farmer = Farmers.objects.get(identity=phone_number)
        self.assertEqual('3a', farmer.current_module)

    def test_wrong_answer_first_question(self):
        helper_list = [
            ('205', '1',
             'No, the reddening of the skin IS a '
             'sign as well as high temperature and Diarrhoea'),
            ('206', '2',
             'No, high temperature IS a sign '
             'as well as the reddening of the skin and Diarrhoea'),
            (
                '207',
                '3',
                'No, Diarrhoea Is a sign as well as the '
                'reddening of the skin and high temperature'
            )
        ]
        for i in helper_list:
            client = IvrTestClient(i[0], str(uuid.uuid4()),
                                   base_url=self.live_server_url)

            client.send('')  # dial in
            resp = client.send(i[1])  # enter incorrect answer
            self.assertEqual(
                END_CONVERSATION_FORMAT.format(text=i[2]),
                resp.content.decode()
            )

    def test_correct_with_wrong_answer(self):
        phone_number = '201'

        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)

        client.send('')  # dial in
        client.send('4')   # enter correct answer
        resp = client.send('2')  # incorrect answer

        expected_text = "No, reporting to the District " \
                        "veterinary Office will help stop the " \
                        "spread of African swine fever. " \
                        "Can African swine fever be " \
                        "treated or vaccinated against? " \
                        "1. Yes " \
                        "2. No"
        self.assertEqual(
            CONVERSATION_FORMAT.format(text=expected_text),
            resp.content.decode()
        )

        resp = client.send('2')  # enter the correct answer

        expected_text = 'Yes, African swine fever has no cure, ' \
                        'no vaccine and no treatment'

        self.assertEqual(
            END_CONVERSATION_FORMAT.format(text=expected_text),
            resp.content.decode()
        )

        # ensure farmer has been moved to the next module
        farmer = Farmers.objects.get(identity=phone_number)
        self.assertEqual('3a', farmer.current_module)

    def test_correct_with_two_wrong(self):
        phone_number = '203'

        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)

        client.send('')  # dial in
        client.send('4')  # enter the correct answer
        client.send('2')  # enter incorrect answer
        resp = client.send('1')  # enter incorrect answer again

        expected_text = "No, African swine fever has no cure, " \
                        "no vaccine and no treatment"

        self.assertEqual(
            END_CONVERSATION_FORMAT.format(text=expected_text),
            resp.content.decode()
        )
