from django.test import LiveServerTestCase
import uuid
from .util import IvrTestClient, \
    END_PLAY_CONVERSATION_FORMAT
from core.models import Farmers
from django.conf import settings


class TestingASF6(LiveServerTestCase):
    maxDiff = None

    first_conversation = settings.USSD_CLIPS_ENDPOINT + "asf6.wav"

    def setUp(self):
        # register all numbers we are going to use
        for i in (200, 201):
            Farmers.objects.create(
                identity=str(i),
                current_module=6
            )

    def test(self):
        phone_number = '200'

        client = IvrTestClient(phone_number, str(uuid.uuid4()),
                               base_url=self.live_server_url)
        # dial in
        ivr_resp = client.send('')

        self.assertEqual(
            END_PLAY_CONVERSATION_FORMAT.format(text=self.first_conversation),
            ivr_resp.content.decode()
        )

        # ensure farmer has been moved to the next module
        farmer = Farmers.objects.get(identity=phone_number)
        self.assertEqual('7', farmer.current_module)