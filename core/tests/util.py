from django.urls import reverse
from requests import post

CONVERSATION_FORMAT = '<?xml version="1.0" encoding="UTF-8"?>' \
                      '<Response>' \
                      '<GetDigits timeout="10" finishOnKey="#">' \
                      '<Say>{text}</Say>' \
                      '</GetDigits>' \
                      '<GetDigits timeout="10" finishOnKey="#">' \
                      '<Say>{text}</Say>' \
                      '</GetDigits>' \
                      '<GetDigits timeout="10" finishOnKey="#">' \
                      '<Say>{text}</Say>' \
                      '</GetDigits>' \
                      '</Response>'

END_CONVERSATION_FORMAT = '<?xml version="1.0" encoding="UTF-8"?>' \
                          '<Response><Say>{text}</Say></Response>'


PLAY_CONVERSATION_FORMAT = '<?xml version="1.0" encoding="UTF-8"?>' \
                           '<Response>' \
                           '<GetDigits timeout="10" finishOnKey="#">' \
                           '<Play url="{text}"/>' \
                           '</GetDigits>' \
                           '<GetDigits timeout="10" finishOnKey="#">' \
                           '<Play url="{text}"/>' \
                           '</GetDigits>' \
                           '<GetDigits timeout="10" finishOnKey="#">' \
                           '<Play url="{text}"/>' \
                           '</GetDigits>' \
                           '</Response>'

END_PLAY_CONVERSATION_FORMAT = '<?xml version="1.0" encoding="UTF-8"?>' \
                               '<Response>' \
                               '<Play url="{text}"/>' \
                               '</Response>'


class IvrTestClient(object):

    def __init__(self, phone_number,
                 session_id, base_url, language='en'):
        self.phone_number = phone_number
        self.session_id = session_id
        self.language = language
        self.base_url = base_url

    def send(self, input_text):
        url = self.base_url + reverse('ivr_view')
        resp = post(url, json=dict(
            callerNumber=self.phone_number,
            sessionId=self.session_id,
            dtmfDigits=input_text
        ))
        return resp
