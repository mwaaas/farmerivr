#!/usr/bin/env bash

[ -e celerybeat.pid ] && rm -- celerybeat.pid

celery -A farmerIvr beat -l info