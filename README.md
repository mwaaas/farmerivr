# Install Docker and docker compose
Docker installation: https://docs.docker.com/install/

Docker compose installation: https://docs.docker.com/compose/install/


# To run the application use the following command
docker-compose up
The project will be up and running. 


# Requirements to understand the project
- Good knowledge of the following:
    - Python programming language (https://www.python.org/)
    - Django web framework (https://www.djangoproject.com/)
    - Ussd airflow project  (http://django-ussd-airflow.readthedocs.io/en/latest/#)
    
- Knowing docker is optional -> its not required. 